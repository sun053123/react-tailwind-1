import React from "react"
import Navbar from "./components/Navbar"
import Hero from "./components/Hero"

function App() {

  return (
    <div className="App">
      <Navbar />
      <Hero />
      <h1>
        Hi
      </h1>
    </div>
  )
}

export default App
